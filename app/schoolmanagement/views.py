from django.shortcuts import render,redirect

from django.core.mail import EmailMessage
from django.conf import settings
from bs4 import BeautifulSoup as bs
from app.about.models import *
from app.contact.models import ContactDetail, Map
from app.schoolmanagement.models import *
from urllib.parse import urlparse
from django.conf import settings
from django.http import HttpResponseRedirect
from django.urls.base import resolve, reverse
from django.urls.exceptions import Resolver404
from django.utils import translation
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
# Create your views here.

def dashboard(request):
    programlist = Programs.objects.all().order_by('-id')[:3]
    facilitielist =Facilities.objects.all().order_by('-id')[:3]
    eventlist =Event.objects.all().order_by('-id')[:2]
    popUpImage=ImagePopUp.objects.all().order_by('-id')[:1]
    bannerlist =Carousel.objects.all()
    aboutlist = Aboutus.objects.all().order_by('-id')[:1]  # Retrieve the first Aboutus object from the queryset
    notices =Notice.objects.all().order_by('-id')[:3]

    teamlist = TeamMemeber.objects.all()[:4]
    context = {
        'teamlist': teamlist,
        'programlist':programlist,
        'facilitielist':facilitielist,
        'eventlist':eventlist,
        'aboutlist':aboutlist,  
        'bannerlist':bannerlist,
        'popUpImage':popUpImage,
        'notices':notices

    }
    return render(request,'pages/index.html',context)


def aboutus(request):
    aboutlist = Aboutus.objects.all() # Retrieve the first Aboutus object from the queryset
    sociallist = SocialNetwork.objects.all()[:4]
    teamlist = TeamMemeber.objects.all()[:4]
    context = {
        'aboutlist': aboutlist,  # Pass the Aboutus object to the context instead of the queryset
        'teamlist': teamlist,
        'sociallist': sociallist,
    }
    
    return render(request, 'pages/about.html', context)

def contactus(request):
    contactlist = ContactDetail.objects.all()
    maplist = Map.objects.all()
    if request.method == 'POST':
        name = request.POST.get('name')
        email = request.POST.get('email')
        number = request.POST.get('number')
        messages = request.POST.get('messages')
        message_subject = request.POST.get('message_subject')
        subject = 'New Contact Form Submission'
        message = f'You have received a new contact form submission:\n\nName: {name}\nEmail: {email}\nPhone: {number}\nMessage: {messages}\nMessage Subject: {message_subject}'

        email_message = EmailMessage(
            '',
            message,
            email,
            settings.EMAIL_RECEIVING_USER,
            reply_to=[email],
        )
        email_message.subject = subject
        email_message.send()

        return redirect('contactus')
    context={
        'contactlist':contactlist,
        'maplist':maplist
    }
    return render(request, "pages/contact.html",context)


def gallery(request):
    gallerytype_list = GalleryType.objects.all().order_by('-id')[:5]
    gallery_images =GalleryImage.objects.all()
    context={
        'gallerytype_list':gallerytype_list,
        'gallery_images':gallery_images
    }
    return render(request,"pages/gallery.html",context)


def singleprogram(request,slug):
    programlist =Programs.objects.get(slug=slug)
    programlists =Programs.objects.all().exclude(slug=slug)
    context={
        'programlist':programlist,
        'programlists':programlists

    }
    return render(request, "pages/programsingle.html",context)


def siglefacility(request,slug):
    facilitielist =Facilities.objects.get(slug=slug)
    context={
        'facilitielist':facilitielist

    }
    return render(request, "pages/singlefacility.html",context)

def singleevent(request,slug):
    eventlist =Event.objects.get(slug=slug)
    eventlists =Event.objects.all().exclude(slug=slug)
    context={
        'eventlist':eventlist,
        'eventlists':eventlists

    }
    return render(request, "pages/singleevent.html",context)


def service(request):
    page = request.GET.get('page', 1)
    service =Programs.objects.all()

    paginator = Paginator(service, 3)
    try:
        servicelist= paginator.page(page)
    except PageNotAnInteger:
        servicelist= paginator.page(1)
    except EmptyPage:
        servicelist= paginator.page(paginator.num_pages)
    context={
        'servicelist':servicelist,
    }
    return render(request, "pages/service.html",context)
  

def event(request):
    page = request.GET.get('page', 1)
    events=Event.objects.all()
    paginator = Paginator(events, 3)
    try:
        eventlist= paginator.page(page)
    except PageNotAnInteger:
        eventlist= paginator.page(1)
    except EmptyPage:
        eventlist= paginator.page(paginator.num_pages)
    context={
        'eventlist':eventlist,

    }
    return render(request, "pages/event.html",context)


def facility(request):
    page = request.GET.get('page', 1)
    facility =Facilities.objects.all()
    paginator = Paginator(facility, 3)
    try:
        facilitylist= paginator.page(page)
    except PageNotAnInteger:
        facilitylist= paginator.page(1)
    except EmptyPage:
        facilitylist= paginator.page(paginator.num_pages)
    context={
        'facilitylist':facilitylist,

    }
    return render(request, "pages/facilties.html",context)


def set_language(request, language):
    for lang, _ in settings.LANGUAGES:
        translation.activate(lang)
        try:
            view = resolve(urlparse(request.META.get("HTTP_REFERER")).path)
        except Resolver404:
            view = None
        if view:
            break

    if view:
        translation.activate(language)
        next_url = reverse(view.url_name, args=view.args, kwargs=view.kwargs)
        response = HttpResponseRedirect(next_url)
        response.set_cookie(settings.LANGUAGE_COOKIE_NAME, language)
    else:
        response = HttpResponseRedirect("/")

    return response

def notice(request):
    page = request.GET.get('page', 1)
    notices =Notice.objects.all()
    paginator = Paginator(notices, 3)
    try:
        noticelist= paginator.page(page)
    except PageNotAnInteger:
        noticelist= paginator.page(1)
    except EmptyPage:
        noticelist= paginator.page(paginator.num_pages)
    context={
        'noticelist':noticelist,

    }
    return render(request, "pages/notice.html",context)


def singlenotice(request,slug):
    singlenotice =Notice.objects.get(slug=slug)
    noticelists =Notice.objects.all().exclude(slug=slug)
    context={
        'singlenotice':singlenotice,
        'noticelists':noticelists

    }
    return render(request, "pages/noticesingle.html",context)