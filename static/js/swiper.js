const carouselItems = document.querySelectorAll('.carousel-item');
const carouselIndicators = document.querySelectorAll('.carousel-indicators li');
const prevButton = document.querySelector('.carousel-control-prev');
const nextButton = document.querySelector('.carousel-control-next');
let currentIndex = 0;

// Function to handle carousel indicator click
function handleIndicatorClick(e) {
  const targetIndex = Number(e.target.getAttribute('data-slide-to'));
  updateCarousel(targetIndex);
}

// Function to update carousel
function updateCarousel(index) {
  carouselItems.forEach((item, i) => {
    item.classList.remove('active');
    carouselIndicators[i].classList.remove('active');
  });

  carouselItems[index].classList.add('active');
  carouselIndicators[index].classList.add('active');
  currentIndex = index;
}

// Function to handle previous button click
function handlePrevClick() {
  const newIndex = currentIndex === 0 ? carouselItems.length - 1 : currentIndex - 1;
  updateCarousel(newIndex);
}

// Function to handle next button click
function handleNextClick() {
  const newIndex = currentIndex === carouselItems.length - 1 ? 0 : currentIndex + 1;
  updateCarousel(newIndex);
}

// Function to automatically change images
function autoChangeImage() {
  const newIndex = currentIndex === carouselItems.length - 1 ? 0 : currentIndex + 1;
  updateCarousel(newIndex);
}

// Add click event listener to carousel indicators
carouselIndicators.forEach(indicator => {
  indicator.addEventListener('click', handleIndicatorClick);
});

// Add click event listeners to carousel controls
prevButton.addEventListener('click', handlePrevClick);
nextButton.addEventListener('click', handleNextClick);

// Auto change image every 3 seconds
setInterval(autoChangeImage, 7000);