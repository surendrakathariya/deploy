from modeltranslation.translator import TranslationOptions, register

from app.about.models import Aboutus, TeamMemeber, SocialNetwork, GalleryType
from app.contact.models import ContactDetail, ContactUs
from app.schoolmanagement.models import *



@register(Carousel)
class PostTranslationOptions(TranslationOptions):
    fields = ('title','description',)


@register(Programs)
class PostTranslationOptions(TranslationOptions):
    fields = ('title','body')

@register(Event)
class PostTranslationOptions(TranslationOptions):
    fields = ('title','sub_title','location','description','organizer_name')

@register(Facilities)
class PostTranslationOptions(TranslationOptions):
    fields = ('title','body')

@register(Aboutus)
class PostTranslationOptions(TranslationOptions):
    fields = ('body',)

@register(TeamMemeber)
class PostTranslationOptions(TranslationOptions):
    fields = ('name','position',)

@register(SocialNetwork)
class PostTranslationOptions(TranslationOptions):
    fields = ('name',)

@register(GalleryType)
class PostTranslationOptions(TranslationOptions):
    fields = ('name',)
@register(ContactDetail)
class PostTranslationOptions(TranslationOptions):
    fields = ('description',)
@register(ContactUs)
class PostTranslationOptions(TranslationOptions):
    fields = ('name','message_subject','messages')

@register(Notice)
class PostTranslationOptions(TranslationOptions):
    fields = ('title','body')
