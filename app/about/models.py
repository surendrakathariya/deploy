from django.db import models
from ckeditor.fields import RichTextField
# Create your models here.

class Aboutus(models.Model):
    # body = RichTextField(null=True,blank=True)
    body = models.TextField(null=True,blank=True)
    image   =models.ImageField(upload_to = "image")
    # vedio = models.FileField(upload_to='image')

class TeamMemeber(models.Model):
    name = models.CharField(max_length=256,null=True,blank=True)
    position = models.CharField(max_length=256,null=True,blank=True)
    image =models.ImageField(upload_to = "image")
    
    def __str__(self) -> str:
        return self.name
    
class SocialNetwork(models.Model):
    name = models.CharField(max_length=256,null=True,blank=True)
    url = models.URLField(null=True,blank=True)

    def __str__(self) -> str:
        return self.name


class GalleryType(models.Model):
    name = models.CharField(max_length=256,null=True,blank=True)

    def __str__(self) -> str:
        return self.name

class GalleryImage(models.Model):
    gallary_type = models.ForeignKey(GalleryType,on_delete=models.CASCADE)
    image   = models.ImageField(upload_to = "image")


    def __str__(self) -> str:
        return str(self.gallary_type)