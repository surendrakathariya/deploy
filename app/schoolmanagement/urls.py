from django.urls import path
from .views import *

urlpatterns = [
    path('',dashboard,name='dashboard'),
    path('aboutus/',aboutus,name='aboutus'),
    path('contactus/',contactus,name='contactus'),
    path('gallery/',gallery,name='gallery'),
    path('singleprogram/<slug:slug>/',singleprogram,name='singleprogram'),
    path('siglefacility/<slug:slug>/',siglefacility,name='siglefacility'),
    path('singleevent/<slug:slug>/',singleevent,name='singleevent'),
    path('service/',service,name='service'),
    path('event/',event,name='event'),
    path('notice/',notice,name='notice'),
    path('singlenotice/<slug:slug>/',singlenotice,name='singlenotice'),
    path('facility/',facility,name='facility'),
]
